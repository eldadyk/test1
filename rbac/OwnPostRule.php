<?php
namespace app\rbac;

use app\models\User;
use app\models\Post;
use yii\web\NotFoundHttpException;

use yii\rbac\Rule;
use Yii; 

class OwnPostRule extends Rule
{

	public $name = 'OwnPostRule';

	
		public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['post']) ? $params['post']->author == $user : false;
		}
		return false;
	}
		
}
?>